//
//  PeelSDK.h
//  PeelSDK
//
//  Created by Hiteshwar Vadlamudi on 05/08/15.
//  Copyright (c) 2015 Hiteshwar Vadlamudi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
//#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <Parse/Parse.h>
//! Project version number for PeelSDK.
FOUNDATION_EXPORT double PeelSDKVersionNumber;

//! Project version string for PeelSDK.
FOUNDATION_EXPORT const unsigned char PeelSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PeelSDK/PublicHeader.h>


